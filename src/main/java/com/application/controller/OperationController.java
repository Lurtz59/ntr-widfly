package com.application.controller;

import com.application.service.OperationService;
import com.application.service.impl.IOperationService;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class OperationController {

    private OperationService operationService = new IOperationService();

    public String credit(final String numAccount, final Double credit) {
        return operationService.doCredit(numAccount, credit);
    }

    public String debit(final String numAccount, final Double credit) {
        return operationService.doDebit(numAccount, credit);
    }
}
