package com.application.service;

public interface OperationService {

    String doCredit(final String numAccount, final Double credit);

    String doDebit(final String numAccount, final Double credit);
}
