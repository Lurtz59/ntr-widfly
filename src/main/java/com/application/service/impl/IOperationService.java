package com.application.service.impl;

import com.application.config.ErrorMessage;
import com.application.config.SucessMessage;
import com.application.service.OperationService;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class IOperationService implements OperationService {

    @Override
    public String doDebit(final String numAccount, final Double credit) {
        try {
            final String request = "<transactionDTO>" +
                    "           <numberAccount>"+numAccount+"</numberAccount>" +
                    "           <sum>"+credit+"</sum>" +
                    "         </transactionDTO>";
            final String url = "http://localhost:8081/api/operation/debit";
            postRequest(request, url);
            return SucessMessage.PRODUCT_BUY;
        } catch (Exception e) {
            return ErrorMessage.ERROR_500;
        }
    }

    @Override
    public String doCredit(final String numAccount, final Double debit) {
        try {
            final String request = "<transactionDTO>" +
                    "           <numberAccount>"+numAccount+"</numberAccount>" +
                    "           <sum>"+debit+"</sum>" +
                    "         </transactionDTO>";
            final String url = "http://localhost:8081/api/operation/credit";
            postRequest(request, url);
            return SucessMessage.REFUND_BUY;
        } catch (Exception e) {
            return ErrorMessage.ERROR_500;
        }
    }

    private void postRequest(final String request, final String urlS) throws IOException {
        final URL url = new URL(urlS);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set timeout as per needs
        connection.setConnectTimeout(20000);
        connection.setReadTimeout(20000);

        // Set DoOutput to true if you want to use URLConnection for output.
        // Default is false
        connection.setDoOutput(true);

        connection.setUseCaches(true);
        connection.setRequestMethod("POST");

        // Set Headers
        connection.setRequestProperty("Accept", "application/xml");
        connection.setRequestProperty("Content-Type", "application/xml");

        // Write XML
        final OutputStream outputStream = connection.getOutputStream();
        final byte[] b = request.getBytes("UTF-8");
        outputStream.write(b);
        outputStream.flush();
        outputStream.close();

        // Read XML
        InputStream inputStream = connection.getInputStream();
        byte[] res = new byte[2048];
        int i = 0;
        final StringBuilder response = new StringBuilder();
        while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();
    }
}
